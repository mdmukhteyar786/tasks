﻿namespace ProductLibrary
{
    public class Product
    {
        public int pid;
        public string pname;
        public float price;
        public double rating;
    }
}