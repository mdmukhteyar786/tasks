﻿#region Old Datatypes
// See https://aka.ms/new-console-template for more information
//Console.WriteLine("Datatypes : ");
// Implicit Declaration
//Console.WriteLine("Enter Username : ");
//var userName = Console.ReadLine();
//Console.WriteLine("Your username is : "+userName);
// Explicit Declaration
//Console.WriteLine("Enter your Age : ");
//int age = int.Parse(Console.ReadLine());
//Console.WriteLine("Your age is : "+age);
//Console.WriteLine($"Your username is : {userName} \nYour age is : {age}");
#endregion
using ProductLibrary;
using SearchProduct;
using System.ComponentModel;


#region UserLibrary
using UserLibrary;
//User userObj = new User() {id=1, name="Mukhteyar Khan", email="mukhteyar@gmail.com", password="1234"};
////userObj.id = 1;
////userObj.name = "Mukhteyar Khan";
////userObj.email = "mukhteyar@gmail.com";
////userObj.password = "1234";

//Console.WriteLine($"User Id : {userObj.id}\nName : {userObj.name}\nEmail : {userObj.email}\nPassword : {userObj.password}");

//// Revoke or Recall the methode
//string fullName = userObj.getFullName();
//Console.WriteLine(fullName);

//// or

//Console.WriteLine((userObj.getFullName));
#endregion


#region Product
//Product obj = new Product();
//obj.pid = 1;
//obj.pname = "Phone";
//obj.price = 5000;
//obj.rating = 4.5;
//Console.WriteLine($"Product number : {obj.pid}\nProduct Name : {obj.pname}\nProduct Price : {obj.price}\nProduct Rating : {obj.rating}");
#endregion


#region Search Product
Search find = new Search();
find.pid = 1;
find.pname = "Laptop";
find.price = 6000;
find.rating = 4.5;
Console.WriteLine("Enter Product Name : ");
string search = Console.ReadLine();

if(find.pname == search & (find.rating == 4.5 || find.rating > 4.5)){
    Console.WriteLine("Product Found !!\n\n");
    Console.WriteLine($"Product number : {find.pid}\nProduct Name : {find.pname}\nProduct Price : {find.price}\nProduct Rating : {find.rating}");
}
else
{
    Console.WriteLine("Sorry !! Product Not Found !!");
}
#endregion

